package paquete_base;

import java.util.Scanner;

import listas.ListaNumeros;

public class Menu {
	private ListaNumeros lista;
	
	public Menu() {
		lista = new ListaNumeros();
	}
	
	public static void main(String[] args) {
		Menu menu = new Menu();
		Scanner es= new Scanner(System.in);	
		int op;
		do {
			System.out.println("Menu");
			System.out.println("[1]: Agregar numero");
			System.out.println("[2]: Ver lista");
			System.out.println("[3]: Ordenar lista");
			System.out.println("[4]: Salir");
			System.out.print("Opcion: ");
			op = es.nextInt();
			
			switch(op){
				case 1:	
					System.out.println("********************************");
					int op2=1;
					do {
						System.out.print("Digite un numero entero: ");
						long num = es.nextLong();
						menu.agregar(num);
						System.out.print("agregar otro 1:si 2:no --> ");
						op2 = es.nextInt();
					}while (op2!=2);
					System.out.println("********************************");
				break;
				
				case 2:
					System.out.println("********************************");
					System.out.println("Lista numeros: "+menu.getLista());
					System.out.println("********************************");
				break;
				
				case 3:
					System.out.println("********************************");
					menu.getLista().ordernar();
					System.out.println("Lista ordenada: "+menu.getLista());
					System.out.println("********************************");
				break;
			}
		}while (op!=4);
		
	}
	
	public void agregar(long dato) {
		this.lista.add(dato);
	}

	public ListaNumeros getLista() {
		return lista;
	}

	public void setLista(ListaNumeros lista) {
		this.lista = lista;
	}
}
