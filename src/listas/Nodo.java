package listas;

public class Nodo {
	private long dato;
	private Nodo siguiente;
	
	public Nodo(long dato) {
		this.dato 		= dato;
		this.siguiente 	= null;
	}

	public long getDato(){
		return this.dato;
	}
	
	public void setSiguiente(Nodo dato) {
		this.siguiente = dato;
	}
	
	public Nodo getSiguiente() {
		return this.siguiente;
	}

	public void setDato(long dato) {
		this.dato = dato;
	}
	
	@Override
	public String toString() {
		return Long.toString(this.dato);
	}
}
