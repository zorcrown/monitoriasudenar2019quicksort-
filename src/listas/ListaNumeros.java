package listas;

public class ListaNumeros {
	private Nodo cabecera;
	private Nodo fin;
	
	public ListaNumeros(){
		this.cabecera 	= null;
		this.fin  		= null;
	}
	
	public boolean es_vacia() {
		if (this.cabecera  == null) {
			return true;
		}
		return false;
	}
	
	public boolean add(long dato) {
		if (this.es_vacia()) {
			this.cabecera = new Nodo(dato);
			this.fin = this.cabecera;
			return true;
		}
		else{
			this.fin.setSiguiente(new Nodo(dato)); 
			this.fin = this.fin.getSiguiente();
			return true;
		}
	}

	public Nodo getCabecera() {
		return cabecera;
	}

	public void setCabecera(Nodo cabecera) {
		this.cabecera = cabecera;
	}
	
	public int len() {
		Nodo nodo_actual;
		nodo_actual = this.cabecera;
		int cont = 0;
		while (nodo_actual != null) {
			cont+=1;
			nodo_actual = nodo_actual.getSiguiente();
		}
		return cont;
	}
	
	public long buscarDatoPos(long pos) { 
		Nodo nodo_actual;
		nodo_actual = this.cabecera;
		long dato = Integer.MIN_VALUE,cont=0;
		while (nodo_actual != null) {
			if (cont == pos) {
				return nodo_actual.getDato();
			}
			cont+=1;
			nodo_actual = nodo_actual.getSiguiente();
		}
		return dato;
	}
	
	public void modificarDato(long dato,long pos) {
		Nodo nodo_actual;
		nodo_actual = this.cabecera;
		long cont=0;
		while (nodo_actual != null) {
			if (cont == pos) {
				nodo_actual.setDato(dato);
			}
			cont+=1;
			nodo_actual = nodo_actual.getSiguiente();
		}		
	}
	
	@Override
	public String toString() {
		Nodo nodo_actual;
		nodo_actual = this.cabecera;
		String cadena = "";
		while (nodo_actual != null) {
			cadena+=nodo_actual.toString()+" ";
			nodo_actual = nodo_actual.getSiguiente();
		}
		return cadena;
	}
	
	public ListaNumeros ordernar() {
		quicksort(this, 0, this.len()-1);	
		return this;
	}

	private static void quicksort(ListaNumeros ls, long inicio, long fin) {		
		long izquierda, derecha;
		long media, aux,dato;
		izquierda = inicio;
		derecha = fin;
		media = ls.buscarDatoPos((inicio+fin)/2);
		do {
			while (ls.buscarDatoPos(izquierda) < media && izquierda < fin) izquierda++;
			while (media < ls.buscarDatoPos(derecha) && derecha > inicio) derecha--;
			if (izquierda <= derecha) {
				aux = ls.buscarDatoPos(izquierda);
				dato = ls.buscarDatoPos(derecha);
				ls.modificarDato(dato,izquierda);
				ls.modificarDato(aux,derecha);
				izquierda ++;
				derecha--;
			}
		}while (izquierda <= derecha);
		if (inicio < derecha) quicksort(ls, inicio, derecha);
		if (izquierda < fin) quicksort(ls, izquierda, fin);			
	}

}
